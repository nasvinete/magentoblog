<?php
namespace Study\Blog\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\RouterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Url;
use Magento\Framework\UrlInterface;
use Study\Blog\Model\CategoryFactory;
use Study\Blog\Model\PostFactory;

class Router implements RouterInterface
{
    /**
     * @var PostFactory
     */
    protected $_pageFactory;

    /**
     * @var ActionFactory
     */
    protected $actionFactory;

    /**
     * Event manager
     *
     * @var ManagerInterface
     */
    protected $_eventManager;

    /**
     * Post factory
     *
     * @var PostFactory
     */
    protected $_postFactory;

    /**
     * Category factory
     *
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * Config primary
     *
     * @var \Magento\Framework\App\State
     */
    protected $_appState;

    /**
     * Url
     *
     * @var UrlInterface
     */
    protected $_url;

    /**
     * Response
     *
     * @var ResponseInterface
     */
    protected $_response;

    /**
     * @param ActionFactory     $actionFactory
     * @param ManagerInterface  $eventManager
     * @param UrlInterface      $url
     * @param PostFactory       $postFactory
     * @param CategoryFactory   $categoryFactory
     * @param ResponseInterface $response
     */
    public function __construct(
        ActionFactory $actionFactory,
        ManagerInterface $eventManager,
        UrlInterface $url,
        PostFactory $postFactory,
        CategoryFactory $categoryFactory,
        ResponseInterface $response
    ) {
        $this->actionFactory = $actionFactory;
        $this->_eventManager = $eventManager;
        $this->_url = $url;
        $this->_pageFactory = $postFactory;
        $this->_response = $response;
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param RequestInterface $request
     *
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        $url_key = trim($request->getPathInfo(), '/blog/');
        $url_key = rtrim($url_key, '/');

        /** @var \Study\Blog\Model\Post $post */
        $post = $this->_pageFactory->create();
        $post_id = $post->checkUrlKey($url_key);
        if (!$post_id) {
            return null;
        }

        $request->setModuleName('blog')->setControllerName('view')
            ->setActionName('index')->setParam('post_id', $post_id);
        $request->setAlias(
            Url::REWRITE_REQUEST_PATH_ALIAS, $url_key
        );

        return $this->actionFactory->create(
            'Magento\Framework\App\Action\Forward'
        );
    }
}