<?php

namespace Study\Blog\Controller\Adminhtml\Category;

use Study\Blog\Controller\Adminhtml\Category;

class Index extends Category
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface|null
     */
    public function execute()
    {
        if ($this->getRequest()->getQuery('ajax')) {
            $this->_forward('grid');
            return null;
        }

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Study_Blog::category');
        $resultPage->getConfig()->getTitle()->prepend(__('Category'));
        return $resultPage;
    }
}
