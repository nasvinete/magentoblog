<?php
namespace Study\Blog\Controller\Adminhtml\Category;

use Magento\Backend\App\Action;

class Delete extends Action
{
    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Study_Blog::delete');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('category_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                $model = $this->_objectManager->create(
                    'Study\Blog\Model\Category'
                );
                $model->load($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(
                    __('The item has been deleted.')
                );
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath(
                    '*/*/edit', ['category_id' => $id]
                );
            }
        }
        $this->messageManager->addErrorMessage(
            __('We can\'t find a category to delete.')
        );
        return $resultRedirect->setPath('*/*/');
    }
}
