<?php

namespace Study\Blog\Controller\Adminhtml\Category;

use Study\Blog\Controller\Adminhtml\Category;

class Grid extends Category
{
    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        return $this->_resultPageFactory->create();
    }
}