<?php
namespace Study\Blog\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Study\Blog\Model\ResourceModel\Category\CollectionFactory;

class Sidebar extends Template
{
    /**
     * @var CollectionFactory,
     */
    protected $_categoryCollectionFactory;

    /**
     * Constructor
     *
     * @param Context           $context
     * @param CollectionFactory $categoryCollectionFactory ,
     * @param array             $data
     */

    public function __construct(
        Context $context,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        $this->_categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \Study\Blog\Model\ResourceModel\Category\Collection
     */
    public function getCategories()
    {
        if (!$this->hasData('categories')) {
            /** @var \Study\Blog\Model\ResourceModel\Category\Collection $categories */
            $categories = $this->_categoryCollectionFactory->create();
            $this->setData('categories', $categories);
        }
        return $this->getData('categories');
    }
}