<?php
namespace Study\Blog\Block\Adminhtml\Post\Edit;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Config\Model\Config\Source\Yesno;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Store\Model\System\Store;
use Study\Blog\Model\CategoryFactory;
use Study\Blog\Model\ResourceModel\Category\CollectionFactory;

class Form extends Generic
{

    /**
     * Country options
     *
     * @var Yesno
     */
    public $booleanOptions;

    /**
     * @var Store
     */
    protected $_systemStore;

    /**
     * Category collection factory
     *
     * @var CollectionFactory
     */
    public $categoryCollectionFactory;

    /**
     * Category factory
     *
     * @var CategoryFactory
     */
    public $categoryFactory;

    /**
     * @param Context           $context
     * @param Registry          $registry
     * @param FormFactory       $formFactory
     * @param Store             $systemStore
     * @param Yesno             $booleanOptions
     * @param CategoryFactory   $categoryFactory
     * @param CollectionFactory $categoryCollectionFactory
     * @param array             $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        Store $systemStore,
        Yesno $booleanOptions,
        CategoryFactory $categoryFactory,
        CollectionFactory $categoryCollectionFactory,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        $this->booleanOptions = $booleanOptions;
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Retrieve list of categories
     *
     * @return array
     */
    protected function _getCategories()
    {
        $categoryItems = $this->categoryFactory->create()->getCollection()
            ->getItems();
        $groups = [];
        foreach ($categoryItems as $category) {
            /** @var \Study\Blog\Model\Category $category */
            $groups[] = ['label' => $category->getTitle(),
                         'value' => $category->getId()];
        }
        return $groups;
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('post_form');
        $this->setTitle(__('Post Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        /** @var \Study\Blog\Model\Post $model */
        $model = $this->_coreRegistry->registry('blog_post');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id'     => 'edit_form',
                        'action' => $this->getData('action'),
                        'method' => 'post']]
        );

        $form->setHtmlIdPrefix('post_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getPostId()) {
            $fieldset->addField('post_id', 'hidden', ['name' => 'post_id']);
        }

        $fieldset->addField(
            'title',
            'text',
            ['name'  => 'title', 'label' => __('Post Title'),
             'title' => __('Post Title'), 'required' => true]
        );

        $fieldset->addField(
            'url_key',
            'text',
            [
                'name'     => 'url_key',
                'label'    => __('URL Key'),
                'title'    => __('URL Key'),
                'required' => true,
                'class'    => 'validate-xml-identifier'
            ]
        );

        $fieldset->addField(
            'enabled',
            'select',
            [
                'label'    => __('Enabled'),
                'title'    => __('Enabled'),
                'name'     => 'enabled',
                'required' => true,
                'values'   => $this->booleanOptions->toOptionArray(),
            ]
        );
        if (!$model->getId()) {
            $model->setData('enabled', '1');
        }

        $fieldset->addField(
            'category_id',
            'select',
            [
                'label'    => __('Category'),
                'title'    => __('Category'),
                'name'     => 'category_id',
                'required' => true,
                'value'    => $model->getCategoryId(),
                'values'   => $this->_getCategories(),
            ]
        );
        if (!$model->getId()) {
            $model->setData('category_id', '1');
        }

        $fieldset->addField(
            'store_ids',
            'multiselect',
            [
                'name'     => 'store_ids',
                'label'    => __('Store Views'),
                'title'    => __('Store Views'),
                'note'     => __('Select Store Views'),
                'required' => true,
                'values'   => $this->_systemStore->getStoreValuesForForm(
                    false, true
                ),
            ]
        );

        $fieldset->addField(
            'content',
            'editor',
            [
                'name'     => 'content',
                'label'    => __('Content'),
                'title'    => __('Content'),
                'style'    => 'height:36em',
                'required' => true
            ]
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}
