<?php
namespace Study\Blog\Block;

use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Study\Blog\Api\Data\PostInterface;
use Study\Blog\Model\Post;
use Study\Blog\Model\ResourceModel\Post\Collection as PostCollection;
use Study\Blog\Model\ResourceModel\Post\CollectionFactory;

class PostList extends Template implements IdentityInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_postCollectionFactory;

    /**
     * Construct
     *
     * @param Context           $context
     * @param CollectionFactory $postCollectionFactory ,
     * @param array             $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $postCollectionFactory,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_postCollectionFactory = $postCollectionFactory;
    }

    /**
     * @return \Study\Blog\Model\ResourceModel\Post\Collection
     */
    public function getPosts()
    {
        if (!$this->hasData('posts')) {
            /** @var \Study\Blog\Model\ResourceModel\Post\Collection $posts */
            $posts = $this->_postCollectionFactory
                ->create()
                ->addFilter('enabled', 1)
                ->addOrder(
                    PostInterface::CREATION_TIME,
                    PostCollection::SORT_ORDER_DESC
                );
            if ($this->getRequest()->getParam('category')) {
                $posts->addFilter(
                    '`main_table`.`category_id`',
                    (int)$this->getRequest()->getParam('category')
                );//todo check it;
            }
            $this->setData('posts', $posts);
        }
        return $this->getData('posts');
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [Post::CACHE_TAG . '_' . 'list'];
    }
}
