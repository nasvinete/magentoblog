<?php namespace Study\Blog\Helper;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\View\Result\PageFactory;

class Category extends AbstractHelper
{

    /**
     * @var \Study\Blog\Model\Category
     */
    protected $_category;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param Context                    $context
     * @param \Study\Blog\Model\Category $category
     * @param PageFactory                $resultPageFactory
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Context $context,
        \Study\Blog\Model\Category $category,
        PageFactory $resultPageFactory
    ) {
        $this->_category = $category;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Return a blog category from given category id.
     *
     * @param Action $action
     * @param null   $categoryId
     *
     * @return \Magento\Framework\View\Result\Page|bool
     */
    public function prepareResultCategory(Action $action, $categoryId = null)
    {
        if ($categoryId !== null && $categoryId !== $this->_category->getId()) {
            $delimiterPosition = strrpos($categoryId, '|');
            if ($delimiterPosition) {
                $categoryId = substr($categoryId, 0, $delimiterPosition);
            }

            if (!$this->_category->load($categoryId)) {
                return false;
            }
        }

        if (!$this->_category->getId()) {
            return false;
        }

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->addHandle('blog_category_view');
        $resultPage->addPageLayoutHandles(['id' => $this->_category->getId()]);

        $this->_eventManager->dispatch(
            'study_blog_category_render',
            ['category' => $this->_category, 'controller_action' => $action]
        );

        return $resultPage;
    }
}
