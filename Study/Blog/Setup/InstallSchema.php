<?php namespace Study\Blog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $installer = $setup;

        $installer->startSetup();

        if (!$installer->tableExists('study_category')) {
            $table = $installer->getConnection()->newTable(
                $installer->getTable('study_category')
            )
                ->addColumn(
                    'category_id', Table::TYPE_INTEGER, null,
                    ['identity' => true, 'nullable' => false,
                     'primary'  => true,], 'Category ID'
                )
                ->addColumn(
                    'title', Table::TYPE_TEXT, 255, ['nullable => false'],
                    'Category Name'
                )
                ->addColumn(
                    'description', Table::TYPE_TEXT, '64k', [],
                    'Category Description'
                )
                ->setComment('Category Table');
            $installer->getConnection()->createTable($table);
        }

        if (!$installer->tableExists('study_blog_post')) {
            $table = $installer->getConnection()
                ->newTable($installer->getTable('study_blog_post'))
                ->addColumn(
                    'post_id', Table::TYPE_INTEGER, null,
                    ['identity' => true, 'nullable' => false,
                     'primary'  => true], 'Post ID'
                )
                ->addColumn(
                    'url_key', Table::TYPE_TEXT, 100,
                    ['nullable' => true, 'default' => null]
                )
                ->addColumn(
                    'title', Table::TYPE_TEXT, 255, ['nullable' => false],
                    'Blog Title'
                )
                ->addColumn(
                    'content', Table::TYPE_TEXT, '2M', [], 'Blog Content'
                )
                ->addColumn(
                    'enabled', Table::TYPE_SMALLINT, null,
                    ['nullable' => false, 'default' => '1'], 'Is Enabled'
                )
                ->addColumn(
                    'creation_time', Table::TYPE_TIMESTAMP, null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'update_time', Table::TYPE_TIMESTAMP, null,
                    ['nullable' => false,
                     'default'  => Table::TIMESTAMP_INIT_UPDATE], 'Updated At'
                )
                ->addColumn(
                    'category_id', Table::TYPE_INTEGER, null,
                    ['nullable' => false], 'Category ID'
                )
                ->addColumn(
                    'store_ids', Table::TYPE_TEXT, null,
                    ['nullable' => false, 'unsigned' => true,], 'Store Id'
                )
                ->addForeignKey(
                    $installer->getFkName(
                        'study_category', 'category_id', 'study_blog_post',
                        'category_id'
                    ),
                    'category_id',
                    $installer->getTable('study_category'),
                    'category_id',
                    Table::ACTION_CASCADE
                )
                ->setComment('Blog Posts');
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}