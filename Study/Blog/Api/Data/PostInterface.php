<?php
namespace Study\Blog\Api\Data;


interface PostInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const POST_ID = 'post_id';
    const URL_KEY = 'url_key';
    const TITLE = 'title';
    const CONTENT = 'content';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';
    const ENABLED = 'enabled';
    const CATEGORY_ID = 'category_id';
    const CATEGORY_TITLE = 'category_title';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get URL Key
     *
     * @return string
     */
    public function getUrlKey();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Get category id
     *
     * @return integer|null
     */
    public function getCategoryId();

    /**
     * Get category title
     *
     * @return string
     */
    public function getCategoryTitle();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function enabled();

    /**
     * Set ID
     *
     * @param int $id
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setId($id);

    /**
     * Set URL Key
     *
     * @param string $url_key
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setUrlKey($url_key);

    /**
     * Return full URL including base url.
     *
     * @return mixed
     */
    public function getUrl();

    /**
     * Set title
     *
     * @param string $title
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setTitle($title);

    /**
     * Set content
     *
     * @param string $content
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setContent($content);

    /**
     * Set creation time
     *
     * @param string $creationTime
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Set update time
     *
     * @param string $updateTime
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setUpdateTime($updateTime);

    /**
     * Set enabled
     *
     * @param int|bool $enabled
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setEnabled($enabled);

    /**
     * Set category_id
     *
     * @param integer $categoryId
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setCategoryId($categoryId);

    /**
     * Set category title
     *
     * @param string $categoryTitle
     *
     * @return \Study\Blog\Api\Data\PostInterface
     */
    public function setCategoryTitle($categoryTitle);
}
