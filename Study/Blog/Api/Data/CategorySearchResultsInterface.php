<?php
namespace Study\Blog\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface for cms page search results.
 *
 * @api
 */
interface CategorySearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get pages list.
     *
     * @return \Study\Blog\Api\Data\CategoryInterface[]
     */
    public function getItems();

    /**
     * Set pages list.
     *
     * @param \Study\Blog\Api\Data\CategoryInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}