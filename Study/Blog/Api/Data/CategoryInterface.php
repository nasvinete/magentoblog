<?php
namespace Study\Blog\Api\Data;

interface CategoryInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CATEGORY_ID = 'category_id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get title
     *
     * @return string|null
     */
    public function getTitle();

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get category Id
     *
     * @return int|null
     */
    public function getCategoryId();

    /**
     * Return full URL including base url.
     *
     * @return mixed
     */
    public function getUrl();

    /**
     * Set ID
     *
     * @param int $id
     *
     * @return \Study\Blog\Api\Data\CategoryInterface
     */
    public function setId($id);

    /**
     * Set title
     *
     * @param string $title
     *
     * @return \Study\Blog\Api\Data\CategoryInterface
     */
    public function setTitle($title);

    /**
     * Set description
     *
     * @param string $description
     *
     * @return \Study\Blog\Api\Data\CategoryInterface
     */
    public function setDescription($description);

    /**
     * Set categoryId
     *
     * @param int $categoryId
     *
     * @return \Study\Blog\Api\Data\CategoryInterface
     */
    public function setCategoryId($categoryId);
}