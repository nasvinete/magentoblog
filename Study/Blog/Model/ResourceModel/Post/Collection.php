<?php
namespace Study\Blog\Model\ResourceModel\Post;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'post_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            'Study\Blog\Model\Post', 'Study\Blog\Model\ResourceModel\Post'
        );
    }

    protected function _renderFiltersBefore()
    {
        $objectManager = ObjectManager::getInstance();
        $storeManager = $objectManager->get(
            '\Magento\Store\Model\StoreManagerInterface'
        );//todo check it;
        $joinTable = $this->getTable('study_category');
        $this->getSelect()->joinLeft(
            $joinTable . ' as study_category',
            '`main_table`.`category_id` = `study_category`.`category_id`',
            array('category_title' => 'title')
        );
        $this->getSelect()->where(
            'FIND_IN_SET(' . $storeManager->getStore()->getStoreId()
            . ',`main_table`.`store_ids`)'
        )->orWhere('FIND_IN_SET(0,`main_table`.`store_ids`)');
        parent::_renderFiltersBefore();
    }
}