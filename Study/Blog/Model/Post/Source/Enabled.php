<?php
namespace Study\Blog\Model\Post\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Study\Blog\Model\Post;

class Enabled implements OptionSourceInterface
{
    /**
     * @var Post
     */
    protected $post;

    /**
     * Constructor
     *
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options[] = ['label' => '', 'value' => ''];
        $availableOptions = $this->post->getAvailableStatuses();
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
